<?php

namespace App\Http\Controllers;

use App\Http\Requests\Click\CompletePaymentRequest;
use App\Http\Requests\Click\PreparePaymentRequest;
use App\Http\Requests\Click\VerifyPaymentRequest;
use App\Http\Services\Click;
use Illuminate\Support\Facades\Log;
use Throwable;

class ClickController extends Controller
{
    public function __construct(private Click $clickService)
    {
    }

    public function prepare(PreparePaymentRequest $request)
    {
        try {
            return $this->clickService->createCardToken(
                $request->input('card_number'),
                $request->input('card_expire'),
            );
        } catch (Throwable $exception) {
            Log::error('CLICK Service: Unable to create card token', [
                'card_number' => $request->input('card_number'),
                'exception' => $exception,
            ]);

            return $this->errorResponse($exception->getMessage());
        }
    }

    public function verify(VerifyPaymentRequest $request)
    {
        try {
            return $this->clickService->verifyCardToken(
                $request->input('card_token'),
                $request->input('sms_code'),
            );
        } catch (Throwable $exception) {
            Log::error('CLICK Service: Unable to verify card token', [
                'exception' => $exception,
            ]);

            return $this->errorResponse($exception->getMessage());
        }
    }

    public function complete(CompletePaymentRequest $request)
    {
        try {
            return $this->clickService->makePaymentWithToken(
                $request->input('card_token'),
                $request->input('amount'),
                $request->input('merchant_trans_id'),
            );
        } catch (Throwable $exception) {
            Log::error('CLICK Service: Unable to make payment with token', [
                'transaction_id' => $request->input('merchant_trans_id'),
                'exception' => $exception,
            ]);

            return $this->errorResponse($exception->getMessage());
        }
    }
}
