<?php

namespace App\Http\Controllers;

use App\Http\Requests\Payme\CompleteRequest;
use App\Http\Requests\Payme\PrepareRequest;
use App\Http\Services\PaymeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Throwable;

class PaymeController extends Controller
{
    public function __construct(private PaymeService $service)
    {
    }

    public function prepare(PrepareRequest $request): JsonResponse
    {
        try {
            return $this->service->prepare(
                $request->input('card_number'),
                $request->input('card_expire'),
                $request->input('amount'),
            );
        } catch (Throwable $exception) {
            Log::error('PAYME Service: payment failed at prepare.', [
                'card_number' => $request->input('card_number'),
                'exception' => $exception,
            ]);

            return $this->errorResponse($exception->getMessage());
        }
    }

    public function complete(CompleteRequest $request): JsonResponse
    {
        try {
            return $this->service->complete(
                $request->input('transaction_id'),
                $request->input('token_id'),
                $request->input('sms_code'),
            );
        } catch (Throwable $exception) {
            Log::error('PAYME Service: payment failed at complete.', [
                'card_number' => $request->input('transaction_id'),
                'exception' => $exception,
            ]);

            return $this->errorResponse($exception->getMessage());
        }
    }
}
