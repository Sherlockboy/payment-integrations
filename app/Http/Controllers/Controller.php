<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function errorResponse(string $message, int $code = 400): JsonResponse
    {
        return response()->json([
            'error' => $message
        ], $code);
    }

    protected function successResponse(array $data = []): JsonResponse
    {
        return response()->json($data);
    }
}
