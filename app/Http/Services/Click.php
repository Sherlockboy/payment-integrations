<?php

namespace App\Http\Services;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class Click
{
    private PendingRequest $client;

    private int $service_id;

    public function __construct()
    {
        $this->service_id = config('services.click.service_id');
        $this->client = Http::baseUrl(config('services.click.base_url'))
            ->asJson()
            ->acceptJson()
            ->withDigestAuth(
                config('services.click.merchant_user_id'),
                config('services.click.secret_key'),
            );
    }

    /**
     * @throws RequestException
     */
    public function createCardToken(string $card_number, string $expire_date)
    {
        return $this->client->post('card_token/request', [
            'service_id' => $this->service_id,
            'card_number' => $card_number,
            'expire_date' => $expire_date,
            'temporary' => 1
        ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function verifyCardToken(string $card_token, int $sms_code)
    {
        return $this->client->post('card_token/verify', [
            'service_id' => $this->service_id,
            'card_token' => $card_token,
            'sms_code' => $sms_code
        ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function makePaymentWithToken(
        string $card_token,
        float  $amount,
        string    $merchant_trans_id,
    )
    {
        return $this->client->post('card_token/payment', [
            'service_id' => $this->service_id,
            'card_token' => $card_token,
            'amount' => $amount,
            'transaction_parameter' => $merchant_trans_id
        ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function createInvoice(
        float  $amount,
        string $phone_number,
        int    $merchant_trans_id,
    )
    {
        return $this->client
            ->post('invoice/create', [
                'service_id' => $this->service_id,
                'amount' => $amount,
                'phone_number' => $phone_number,
                'merchant_trans_id' => $merchant_trans_id
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function checkInvoiceStatus(int $invoice_id)
    {
        return $this->client
            ->get("invoice/status/$this->service_id/$invoice_id")
            ->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function checkPaymentStatus(int $payment_id)
    {
        return $this->client
            ->get("payment/status/$this->service_id/$payment_id")
            ->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function checkPaymentStatusByTransactionId(int $merchant_trans_id)
    {
        return $this->client
            ->get("payment/status_by_mti/$this->service_id/$merchant_trans_id")
            ->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function cancelPayment($payment_id)
    {
        return $this->client
            ->delete("payment/reversal/$this->service_id/$payment_id")
            ->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function deleteCardToken(string $card_token)
    {
        return $this->client
            ->delete("card_token/$this->service_id/$card_token")
            ->throw()->json();
    }
}
