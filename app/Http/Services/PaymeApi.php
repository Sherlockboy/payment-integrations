<?php

namespace App\Http\Services;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class PaymeApi
{
    private PendingRequest $client;

    public function __construct()
    {
        $this->client = Http::baseUrl(config('services.payme.base_url'))
            ->withHeaders([
                'Host' => config('services.payme.host'),
                'Content-Type' => 'application/json',
                'Cache-Control' => 'no-cache'
            ]);
    }

    /**
     * @throws RequestException
     */
    public function createToken(string $card_number, string $card_expire)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderSimple()])
            ->post('/', [
                'id' => 1,
                'method' => 'cards.create',
                'params' => [
                    'card' => [
                        'number' => $card_number,
                        'expire' => $card_expire
                    ],
                    'save' => true
                ]
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function getCodeForVerification(string $token)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderSimple()])
            ->post('/', [
                'id' => 1,
                'method' => 'cards.get_verify_code',
                'params' => [
                    'token' => $token
                ]
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function verifyTokenByCode(string $token, int $code)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderSimple()])
            ->post('/', [
                'id' => 1,
                'method' => 'cards.verify',
                'params' => [
                    'token' => $token,
                    'code' => $code
                ]
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function receiptsCreate(float $amount)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderFull()])
            ->post('/', [
                'id' => 1,
                'method' => 'receipts.create',
                'params' => [
                    'amount' => $amount * 100,
                    'account' => [
                        'user_id' => 1
                    ]
                ]
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function receiptsPay(int $check_id, string $token)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderFull()])
            ->post('/', [
                'id' => 1,
                'method' => 'receipts.pay',
                'params' => [
                    'id' => $check_id,
                    'token' => $token
                ]
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function removeToken(string $token)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderFull()])
            ->post('/', [
                'id' => 1,
                'method' => 'cards.remove',
                'params' => [
                    'token' => $token
                ]
            ])->throw()->json();
    }

    /**
     * @throws RequestException
     */
    public function receiptsCancel(int $check_id)
    {
        return $this->client
            ->withHeaders(['X-Auth' => $this->xAuthHeaderFull()])
            ->post('/', [
                'id' => 1,
                'method' => 'receipts.cancel',
                'params' => [
                    'id' => $check_id
                ]
            ])->throw()->json();
    }

    protected function xAuthHeaderSimple(): string
    {
        return config('services.payme.id');
    }

    protected function xAuthHeaderFull(): string
    {
        return config('services.payme.id') . ':' . config('services.payme.id');
    }
}
