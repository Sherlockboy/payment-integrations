<?php

namespace App\Http\Services;

use App\Models\Transaction;
use App\Models\UserToken;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\JsonResponse;
use RuntimeException;

class PaymeService
{
    public function __construct(private PaymeApi $paymeApi)
    {
    }

    /**
     * @throws RequestException
     */
    public function prepare(
        string $card_number,
        string $card_expire,
        float  $amount
    ): JsonResponse
    {
        $transaction = Transaction::query()->create([
            'user_id' => 1,
            'amount' => $amount,
        ]);

        $token_response = $this->paymeApi
            ->createToken($card_number, $card_expire);

        $sms_response = $this->paymeApi
            ->getCodeForVerification($token_response['result']['card']['token']);

        if (!$sms_response['result']['sent']) {
            throw new RuntimeException("Failed to send verification code.");
        }

        $user_token = UserToken::query()->create([
            'user_id' => 1,
            'token' => $token_response['result']['card']['token'],
        ]);

        return response()->json([
            'transaction_id' => $transaction->id,
            'token_id' => $user_token->id
        ]);
    }

    /**
     * @throws RequestException
     */
    public function complete(
        int $transaction_id,
        int $token_id,
        int $sms_code,
    ): JsonResponse
    {
        $token = UserToken::query()->findOrFail($token_id);

        $verify_response = $this->paymeApi
            ->verifyTokenByCode($token->token, $sms_code);

        if ($verify_response['result']['card']['verify']) {
            throw new RuntimeException("Failed to verify token.");
        }

        $token->update(['is_verified' => true]);

        $transaction = Transaction::query()->findOrFail($transaction_id);

        $receipt_response = $this->paymeApi->receiptsCreate($transaction->amount);

        $pay_response = $this->paymeApi->receiptsPay(
            $receipt_response['result']['receipt']['_id'],
            $token->token,
        );

        return response()->json([
            'message' => 'success',
            'pay_time' => $pay_response['result']['receipt']['pay_time']
        ]);
    }
}
