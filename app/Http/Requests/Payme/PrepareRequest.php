<?php

namespace App\Http\Requests\Payme;

use Illuminate\Foundation\Http\FormRequest;

class PrepareRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_number' => 'required|numeric|digits:16',
            'card_expire' => 'required|numeric|digits:4',
            'amount' => 'required|numeric'
        ];
    }
}
