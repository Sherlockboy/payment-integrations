<?php

namespace App\Http\Requests\Click;

use Illuminate\Foundation\Http\FormRequest;

class CompletePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_token' => 'required|string',
            'amount' => 'required|numeric',
            'merchant_trans_id' => 'required|string'
        ];
    }
}
