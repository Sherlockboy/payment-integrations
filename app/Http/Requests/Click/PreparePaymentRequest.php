<?php

namespace App\Http\Requests\Click;

use Illuminate\Foundation\Http\FormRequest;

class PreparePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_number' => 'required|string',
            'card_expire' => 'required|string'
        ];
    }
}
