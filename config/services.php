<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'click' => [
        'merchant_id' => env('CLICK_MERCHANT_ID', ''),
        'service_id' => env('CLICK_SERVICE_ID', ''),
        'merchant_user_id' => env('CLICK_MERCHANT_USER_ID', ''),
        'secret_key' => env('CLICK_SECRET_KEY', ''),
        'base_url' => env('CLICK_BASE_URL', 'https://api.click.uz/v2/merchant/'),
    ],

    'payme' => [
        'base_url' => env('PAYME_BASE_URL', 'https://checkout.paycom.uz/api'),
        'host' => env('PAYME_HOST', 'checkout.paycom.uz'),
        'id' => env('PAYME_ID', ''),
        'password' => env('PAYME_PASSWORD', ''),
    ],

];
