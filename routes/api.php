<?php

use App\Http\Controllers\ClickController;
use App\Http\Controllers\PaymeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('payments')->group(function () {
    Route::prefix('click')->group(function () {
        Route::post('prepare', [ClickController::class, 'prepare']);
        Route::post('verify', [ClickController::class, 'verify']);
        Route::post('complete', [ClickController::class, 'complete']);
    });

    Route::prefix('payme')->group(function () {
        Route::post('prepare', [PaymeController::class, 'prepare']);
        Route::post('complete', [PaymeController::class, 'complete']);
    });
});
